using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera : MonoBehaviour
{
    [SerializeField] GameObject player;
    // Update is called once per frame
    void Update()
    {
        transform.position = new Vector3(
            Mathf.Clamp(player.transform.position.x, -1.2f, 1.2f),
            Mathf.Clamp(player.transform.position.y, -0.65f, 0.65f),
            transform.position.z
            );
    }
}
